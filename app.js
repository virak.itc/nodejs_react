require('rootpath')();

const express = require("express")
const formidable = require('formidable');

const user = require('./app/routes/user.routes')
const number = require('./app/routes/number.routes')


var app = express()

var environmentRoot =  require('path').normalize(__dirname );

app.use(function (req, res, next) {
  /*var err = new Error('Not Found');
   err.status = 404;
   next(err);*/

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

//  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

app.get("/",function(request,response){
response.send("Hello virak!")
})

app.use('/api/v1/user', user)
app.use('/api/v1/number', number)


module.exports = app;