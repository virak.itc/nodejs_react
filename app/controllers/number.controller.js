
'use strict';
const express = require("express")

const router = express.Router();
const formidable = require('formidable');


const Num = require('../models/number.model');

exports.findAll = function(req, res){
  Num.findAll(function(err, num){
    console.log('contoller')
    if(err) res.send(err);
    console.log('res', num)
    res.send({data:num})
  })
}

exports.create = function(req, res){
  const form = formidable({multiples: true})
  form.parse(req, (err, fields, files)=> {
      if(err){
        next(err);
        return;
      }
      if(Object.keys(fields).length != 4){
        res.status(400).send({error:true, message: 'Please provide all required field'});
      }else{
        const new_num = new Num(fields)
        Num.create(new_num, function(err, num){
          if(err) res.send(err);
          res.json({
            error: false, message: "Number added successfully!", data: num
          })
        });
      }   
  })
}

exports.findById = function(req, res){
  Num.findById(req.params.id, function(err, num){
    if(err) res.send(err);
    res.json(num)
  });
}

exports.update = function(req, res){
  const form = formidable({multiples: true})
  form.parse(req, (err, fields, files)=> {
    if(err){
      next(err);
      return;
    }
    if(Object.keys(fields).length == 0){
      res.status(400).send({error:true, message: 'Please provide all required field'});
    }else{
      const new_num = new User(fields)
      Num.update(req.params.id,  new User(new_num), function(err, num){
        if(err) res.send(err);
        res.json({
          error: false, message: "Number update successfully!", data: new_num
        })
      });
    }   
  })
}

exports.delete = function(req, res) {

  Num.delete( req.params.id, function(err, num) {
    if (err)res.send(err);
    res.json({ error:false, message: 'User successfully deleted' });
  });
};