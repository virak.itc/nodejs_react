
'use strict';
const express = require("express")

const router = express.Router();
const formidable = require('formidable');


const User = require('../models/user.model');

exports.findAll = function(req, res){
  User.findAll(function(err, user){
    console.log('contoller')
    if(err) res.send(err);
    console.log('res', user)
    res.send(user)
  })
}

exports.create = function(req, res){
  const form = formidable({multiples: true})
  form.parse(req, (err, fields, files)=> {
      if(err){
        next(err);
        return;
      }
      if(Object.keys(fields).length != 5){
        res.status(400).send({error:true, message: 'Please provide all required field'});
      }else{
        const new_user = new User(fields)
        User.create(new_user, function(err, user){
          if(err) res.send(err);
          res.json({
            error: false, message: "User added successfully!", data: user
          })
        });
      }   
  })
}

exports.findById = function(req, res){
  User.findById(req.params.id, function(err, user){
    if(err) res.send(err);
    res.json(user)
  });
}

exports.update = function(req, res){
  const form = formidable({multiples: true})
  form.parse(req, (err, fields, files)=> {
    if(err){
      next(err);
      return;
    }
    if(Object.keys(fields).length == 0){
      res.status(400).send({error:true, message: 'Please provide all required field'});
    }else{
      const new_user = new User(fields)
      User.update(req.params.id,  new User(new_user), function(err, user){
        if(err) res.send(err);
        res.json({
          error: false, message: "User update successfully!", data: new_user
        })
      });
    }   
  })
}

exports.delete = function(req, res) {

  User.delete( req.params.id, function(err, user) {
    if (err)res.send(err);
    res.json({ error:false, message: 'User successfully deleted' });
  });
};