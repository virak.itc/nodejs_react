

const express = require('express')

const router = express.Router()

const userController = require('../controllers/user.controller');
const formidable = require('formidable');

//Retrieve all users
router.get('/', userController.findAll);

//Create a user
router.post('/', userController.create);

//Find user by id
router.get('/:id', userController.findById);

//Update user
router.put('/:id', userController.update);

//Delete user
router.delete('/:id', userController.delete);


module.exports = router