

const express = require('express')

const router = express.Router()

const numberController = require('../controllers/number.controller');
const formidable = require('formidable');

//Retrieve all users
router.get('/', numberController.findAll);

//Create a user
router.post('/', numberController.create);

//Find user by id
router.get('/:id', numberController.findById);

//Update user
router.put('/:id', numberController.update);

//Delete user
router.delete('/:id', numberController.delete);


module.exports = router