

var dbConn = require('./../../config/db.config');



var Num = function(num){
    this.number = num.number;
    this.price = num.price;
    this.writter_id = num.writter_id;
    this.notes = num.notes;
    this.created_at = new Date();
    this.updated_at = new Date();
}

Num.create = function(newNum, result){
    console.log(newNum)
    dbConn.query("INSERT INTO numbers set ?",newNum, function(err, res){
        if(err){
            console.log("error: ", err)
            result(err, null)
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId)
        }
        
    });
}

Num.findById = function(id, result){
    dbConn.query("SELECT * FROM numbers WHERE id = ?", id, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    })
}


Num.findAll = function(result){
    var i;
    var nums =  new Array();
    var results =  new Array();
    
    for (i = 0; i <= 99; i++) {
        const index = i;
        var data =  new Array();
        dbConn.query("SELECT * FROM numbers WHERE number = ?", i, function(err, res){
            if(err){
                console.log("error: ", err);
                // result(err, null)
                return null;
            }else{
                
                if(res.length > 0){
                    nums =  new Array();
                    for (var item in res) {
                        const max_length = res.length;
                        if (res.hasOwnProperty(item)) {
                            if(JSON.stringify(res[item].number) == index){
                                nums.push(res[item])
                            }
                            if(item == (max_length - 1)){
                                results.push(nums)
                            }
                        }
                        
                    }
                    
                } 
                
                if(index == 99){
                    result(null, results)
                }
            }
        })
        
    }
}

Num.update = function(id, num, result){
    dbConn.query("UPDATE numbers SET number = ?, price = ?, writter_id = ?, notes = ? WHERE id = ?",
    [num.number, num.price, num.writter_id, num.notes, id], function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    }
)
}

Num.delete = function(id, result){
    dbConn.query("DELETE FROM numbers WHERE id = ?", id, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    })
}

module.exports = Num;