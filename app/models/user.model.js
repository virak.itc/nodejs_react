

var dbConn = require('./../../config/db.config');
const generator = require('../_helpers/generator')
const bcrypt = require('bcrypt');



var User = function(user){
    this.name = user.name;
    this.phone_number = user.phone_number;
    this.password = bcrypt.hashSync(user.password, 10)
    this.type = user.type;
    this.image_url = user.image_url;
    this.created_at = new Date();
    this.updated_at = new Date();
}

User.create = function(newUser, result){
    console.log(newUser)
    dbConn.query("INSERT INTO users set ?",newUser, function(err, res){
        if(err){
            console.log("error: ", err)
            result(err, null)
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId)
        }
        
    });
}

User.findById = function(id, result){
    dbConn.query("SELECT * FROM users WHERE id = ?", id, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    })
}

User.findAll = function(result){
    dbConn.query("SELECT * FROM users", function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    })
}

User.update = function(id, user, result){
    dbConn.query("UPDATE users SET name = ?, phone_number = ?, password = ?, type = ? WHERE id = ?",
    [user.name, user.phone_number, user.password, user.image_url, id], function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    }
)
}

User.delete = function(id, result){
    dbConn.query("DELETE FROM users WHERE id = ?", id, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null)
        }else{
            result(null, res)
        }
    })
}

module.exports = User;