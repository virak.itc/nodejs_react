

require('rootpath')();

const http = require('http')
const app = require('./app')
// const socketIo = require("socket.io");

const port = process.env.PORT || 300

const server = http.createServer(app)

console.log("Started application on port %d", 300)

// const io = socketIo(server, {
//     cors: {
//       origin: "http://localhost:19006",
//       methods: ["GET", "POST"]
//     }
//   });

//   io.on("connection", (socket) => {
//     console.log("New client connected");
//     // socket.emit("FromAPI", "world");
//     socket.on("FromAPI", (arg) => {
//       console.log(arg); // world
//       if(arg){
//         console.log(arg[0]._id = arg[0]._id)
//         socket.emit("FromAPI", arg);
//       }
//     });
//   });

server.listen(port)